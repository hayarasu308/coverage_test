import React from "react"
import Click from "./click"
import Prop from "./prop"
import Prop2 from "./prop2"

function App() {
  return (
    <div>
      <div>
        <h1>Hello World.</h1>
        <p>App Test Page.</p>
      </div>
      <Click />
      <Prop str="XXX!!" />
    </div>
  )
}

export default App