import React, { useState } from "react"

function Prop({str}) {
  const [num, setNum] = useState(1)

  const handleClick = () => {
    setNum(num + 2)
  }

  return (
    <div>
      <input type="button" value={`prop is ${str}, num is ${num}`} onClick={handleClick}/>
    </div>
  )

}

export default Prop