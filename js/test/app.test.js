import '@babel/polyfill'
import React from 'react'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import App from '../app'

Enzyme.configure({ adapter: new Adapter() })

describe('app', () => {
  test('app has a "Click"', () => {
    const wrapper = shallow(<App />)
    const buttonWrapper = wrapper.find('Click')

    expect(buttonWrapper).toHaveLength(1)
  })

  test("snapShot", () => {
    const tree = renderer
      .create(<App />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})