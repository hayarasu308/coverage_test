import '@babel/polyfill'
import React from 'react'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Prop from '../prop'

Enzyme.configure({ adapter: new Adapter() })

describe('prop', () => {
  test('prop has a button', () => {
    const wrapper = shallow(<Prop str="X"/>)
    const buttonWrapper = wrapper.find('input[type="button"]')

    expect(buttonWrapper).toHaveLength(1)
  })

  test('prop has prop', () => {
    const wrapper = shallow(<Prop str="X"/>)

    expect(wrapper.find('input').props().value).toBe(`prop is X, num is 1`)
  })

  test("snapShot", () => {
    const tree = renderer
      .create(<Prop str="X"/>)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})