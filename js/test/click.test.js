import '@babel/polyfill'
import React from 'react'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Click from '../click'

Enzyme.configure({ adapter: new Adapter() })

describe('click', () => {
  test('click has a button', () => {
    const wrapper = shallow(<Click />)
    const buttonWrapper = wrapper.find('input[type="button"]')

    expect(buttonWrapper).toHaveLength(1)
  })

  test('update number on click', () => {
    const wrapper = shallow(<Click />)

    expect(wrapper.find('input').props().value).toBe(1)

    wrapper.find('input').simulate('click')
    wrapper.update()

    expect(wrapper.find('input').props().value).toBe(2)

    for (let i = 0; i < 10; i++) wrapper.find('input').simulate('click')
    wrapper.update()

    expect(wrapper.find('input').props().value).toBe(12)
  })

  test("snapShot", () => {
    const tree = renderer
      .create(<Click />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})