import React, { useState } from "react"

function Click() {
  const [num, setNum] = useState(1)

  const handleClick = () => {
    setNum(num + 1)
  }

  return (
    <div>
      <input type="button" value={num} onClick={handleClick}/>
    </div>
  )

}

export default Click