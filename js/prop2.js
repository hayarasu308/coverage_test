import React, { useState } from "react"

function Prop2({str}) {
  const [num, setNum] = useState(1)

  const handleClick = () => {
    setNum(num * 2)
  }

  return (
    <div>
      <input type="button" value={`prop2 is ${str}, num2 is ${num}`} onClick={handleClick}/>
    </div>
  )

}

export default Prop2