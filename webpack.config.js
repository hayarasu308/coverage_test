const path = require('path')

module.exports = {
  mode: "development",
  entry: ['@babel/polyfill', "./js/index.js"],
  output: {
    filename: "bundle.js",
    path: __dirname
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: [/node_modules/],
        loader: 'babel-loader',
        options: {
          presets: [
            "@babel/preset-env", "@babel/react"
          ]
        }
      }
    ]
  },
  devtool: 'source-map'
}